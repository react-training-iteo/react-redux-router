import React from 'react';
import {NavLink} from 'react-router-dom';

const CoreLayout = (props) => (
  <div className="app">
    <header className="app-header">
      <NavLink exact activeClassName="active" className="" to="/"> HOME</NavLink>
      <NavLink activeClassName="active" className="" to="/about"> ABOUT</NavLink>
      <NavLink activeClassName="active" className="" to="/form"> FORM</NavLink>
      <NavLink activeClassName="active" className="" to="/data"> DATA</NavLink>
    </header>
    {props.children}
  </div>
)

export default CoreLayout;