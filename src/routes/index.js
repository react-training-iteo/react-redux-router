import React, { Component } from 'react';
import { Route, BrowserRouter } from 'react-router-dom';

import CoreLayout from '../layouts/CoreLayout';
import Home from './Home';
import About from './About';
import Data from './Data';
import Form from './Form';
import '../styles/App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <CoreLayout>
          <Route exact path="/" render={Home}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/form" component={Form}></Route>
          <Route path="/data" component={Data}></Route>
        </CoreLayout>
      </BrowserRouter>
    );
  }
}

export default App;