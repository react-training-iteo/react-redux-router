import React from 'react';
import {Field, reduxForm} from 'redux-form'; 

const MySpecialInput = (props) => {
  const { input, label, type, meta: { touched, error, warning } } = props;

  return (
    <div className="input-group">
      <input {...input} placeholder={label} type={type}/>
      <div className="input-error">
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )
}


const MySpecialSelect = (props) => {
  const { input, children, meta: { touched, error, warning } } = props;

  return (
    <div className="input-group">
      <select {...input}>
        {children}
      </select>
      <div className="input-error">
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )
}

const FormPage = (props) => {
  const {handleSubmit, dirty, submitFailed} = props;

  const submit = (values) => {
    console.log(values)
    //POST here
  }

  return (
    <div className="page column grey">
      <h1>FORM</h1>
      <form onSubmit={handleSubmit(submit)} className="form">
        <div className="form-group">
          <label>Imię</label>
          <Field type="text" component="input" name="name" />
        </div>
        <div className="form-group">
          <label>Nazwisko</label>
          <Field type="text" component={MySpecialInput} name="surname" />
        </div>
        <div className="form-group">
          <label>Wiek</label>
          <Field component={MySpecialSelect} name="age">
            <option value=""></option>
            <option value={10}>10</option>
            <option value={20}>20</option>
            <option value={30}>30</option>
            <option value={40}>40</option>
            <option value={50}>50</option>
          </Field>
        </div>
        {submitFailed ? <div className="input-error">Jedno z podanych pól zawiera błąd</div> : null}
        <div className="form-group">
          <button disabled={!dirty}>Wyślij</button>
        </div>
      </form>
    </div>
  )
};

const Form = reduxForm({
  form: 'exampleForm',
  validate: (values) => {
    const {surname, name, age} = values;
    let errors = {};

    errors.name = name ? undefined : 'Imię jest wymagane'
    errors.surname = surname ? undefined : 'Nazwisko jest wymagane'
    errors.age = age && age > 10 ? undefined : 'Za mało lat'

    return errors;
  }
})(FormPage)



export default Form;