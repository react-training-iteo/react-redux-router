import React from 'react';
import {Link} from 'react-router-dom';

const TableHeader = () => (
  <div className="table-header">
    <Link to="/data"><h2>Data table</h2></Link>
  </div>
)

export default TableHeader;