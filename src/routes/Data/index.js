import React from 'react';
import {Route} from 'react-router-dom';
import TableHeader from './components/TableHeader';

import Table from './routes/Table';
import Item from './routes/Item';

const Data = () => (
  <div className="data">
    <TableHeader />
    <div className="table-container">
      <Route  path="/data" component={Table} />
      <Route exact path="/data/:id" component={Item} />
    </div>
  </div>
)

export default Data;