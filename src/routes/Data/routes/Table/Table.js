import React from 'react';
import {NavLink} from 'react-router-dom';

const Table = () => (
  <div className="table">
    <NavLink activeClassName="active" to="/data/1" className="table-item"><p>Element 1</p></NavLink>
    <NavLink activeClassName="active" to="/data/2" className="table-item"><p>Element 2</p></NavLink>
    <NavLink activeClassName="active" to="/data/3" className="table-item"><p>Element 3</p></NavLink>
    <NavLink activeClassName="active" to="/data/4" className="table-item"><p>Element 4</p></NavLink>
    <NavLink activeClassName="active" to="/data/5" className="table-item"><p>Element 5</p></NavLink>
    <NavLink activeClassName="active" to="/data/6" className="table-item"><p>Element 6</p></NavLink>
    <NavLink activeClassName="active" to="/data/7" className="table-item"><p>Element 7</p></NavLink>
  </div>
)

export default Table;