import React from 'react';

const Item = (props) => (
  <div className="item">
    <img alt={`img-${Math.random()}`} src={`https://placeimg.com/150/150/${props.match.params.id}`} />
  </div>
)

export default Item;