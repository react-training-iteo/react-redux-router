import {combineReducers} from 'redux';
import counterReducer from './counterReduxer';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  form: formReducer,
  counter: counterReducer
});