export const ADD_ONE = 'ADD_ONE';
export const SUBSTRACT_ONE = 'SUBSTRACT_ONE';

const actions = {
  ADD_ONE: (state, action) => {
    return {
      ...state,
      counter: state.counter + 1
    }
  },
  SUBSTRACT_ONE: (state, action) => {
    return {
      ...state,
      counter: state.counter - 1 
    }
  }
}


const initValue = {
  counter: 0
}

export default (store = initValue, action) => {
  if(actions[action.type]) return actions[action.type](store, action);
  return store;
}